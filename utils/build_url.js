const url = require('url');

module.exports = (options) => {
    let uri = `https://graph.facebook.com/v2.11/${options.id}/`;

    if (options.endpoint === 'engagement') {
        uri += '?fields=engagement'
    } else if (options.endpoint === 'feed') {
        uri += '?fields=feed'
    } else {
        uri += '?fields=category,name,location,about,company_overview,cover,likes,events'
    }

    uri += `&access_token=${options.access_token}`;

    return url.parse(uri);
}
