const express = require('express'),
    helmet = require('helmet'),
    logger = require('morgan'),
    env = process.env.NODE_ENV || 'development',
    config = require('./config')[env],
    app = express(),
    https = require('https'),
    build_url = require('./utils/build_url');

app.use(helmet());
app.use(logger(config.log));

app.get('/:id', (req, res, next) => {
    const access_token = req.headers['fb-access-token'];
    const id = req.params.id;

    const url = build_url({
        endpoint: 'summary',
        id: id,
        access_token: access_token
    });

    https.get(url, (response) => {
        const statusCode = response.statusCode;
        const statusMessage = response.statusMessage;
        const contentType = response.headers['content-type'];

        let error = null;
        if (statusCode !== 200) {
            error = new Error(`Facebook API returned status code ${statusCode} with message '${statusMessage}'.`);
        } 

        if (error) {
            response.resume();
            return res.status(500).json({
                message: error.message
            });
        }

        response.setEncoding('utf8');
        let rawData = '';
        response.on('data', (chunk) => {
            rawData += chunk;
        });

        response.on('end', () => {
            let parsedData = JSON.parse(rawData);
            return res.json({
                data: parsedData
            });
        });
    }).on('error', (err) => {
        console.log(err.message);
    });
});

app.get('/:id/engagement', (req, res, next) => {
    const access_token = req.headers['fb-access-token'];
    const id = req.params.id;

    const url = build_url({
        endpoint: 'engagement',
        id: id,
        access_token: access_token
    });

    https.get(url, (response) => {
        const statusCode = response.statusCode;
        const statusMessage = response.statusMessage;
        const contentType = response.headers['content-type'];

        let error = null;
        if (statusCode !== 200) {
            error = new Error(`Facebook API returned status code ${statusCode} with message '${statusMessage}'.`);
        } 

        if (error) {
            response.resume();
            return res.status(500).json({
                message: error.message
            });
        }

        response.setEncoding('utf8');
        let rawData = '';
        response.on('data', (chunk) => {
            rawData += chunk;
        });

        response.on('end', () => {
            let parsedData = JSON.parse(rawData);
            return res.json({
                data: parsedData
            });
        });
    }).on('error', (err) => {
        console.log(err.message);
    });
});

app.get('/:id/feed', (req, res, next) => {
    const access_token = req.headers['fb-access-token'];
    const id = req.params.id;

    const url = build_url({
        endpoint: 'feed',
        id: id,
        access_token: access_token
    });

    https.get(url, (response) => {
        const statusCode = response.statusCode;
        const statusMessage = response.statusMessage;
        const contentType = response.headers['content-type'];

        let error = null;
        if (statusCode !== 200) {
            error = new Error(`Facebook API returned status code ${statusCode} with message '${statusMessage}'.`);
        } 

        if (error) {
            response.resume();
            return res.status(500).json({
                message: error.message
            });
        }

        response.setEncoding('utf8');
        let rawData = '';
        response.on('data', (chunk) => {
            rawData += chunk;
        });

        response.on('end', () => {
            let parsedData = JSON.parse(rawData);
            return res.json({
                data: parsedData
            });
        });
    }).on('error', (err) => {
        console.log(err.message);
    });
});

app.listen(config.port, () => {
    console.log(`Server listening on port ${config.port}...`);
});
