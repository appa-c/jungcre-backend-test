# Jungle Creations Backend Test

A simple NodeJS API for interacting with Facebook's API, specifically, data on public pages.

## Getting Started

Clone this repository and run locally with `npm start` or `nodemon`.

Alternatively, a live version of the API is available at `https://jungcre-test.herokuapp.com/`

## Usage

The API provides three endpoints, all endpoints require the ID/name of the page you want data for as well as a facebook access token provided via a `fb-access-token` header.

### /{:page_id}

Returns a summary of a company/page.

*Example*

`https://jungcre-test.herokuapp.com/microsoft`

### /{:page_id}/engagement

Returns the engagement data of a page.

*Example*

Uses Microsoft's Facebook ID instead of name.

`https://jungcre-test.herokuapp.com/20528438720/engagement`

### /{:page_id}/feed

Returns the feed for a page.

*Example*

`https://jungcre-test.herokuapp.com/20528438720/feed`

### Writing to MongoDB

Within `mongo.js` set `ACCESS_TOKEN` to your facebook access token, and run with `node mongo.js`

This file makes 5 calls to the summary endpoint, storing the returned data in a local MongoDB `test` database, and `fb` collection.
