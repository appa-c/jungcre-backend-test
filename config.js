const config = {
    development: {
        log: 'dev',
        port: 3000
    },
    production: {
        log: 'combined',
        port: process.env.PORT
    }
};

module.exports = config;
