const MongoClient = require('mongodb').MongoClient,
    https = require('https'),
    ACCESS_TOKEN = '';

const companies = [
    'tesla',
    'microsoft',
    'apple',
    'google',
    'facebook'
];

for (let i = 0, l = companies.length; i < l; i++) {
    makeRequest(companies[i]);
}

function writeToDb(parsedData) {
    MongoClient.connect('mongodb://localhost:27017/', (err, client) => {
        const db = client.db('test');
        if (err) {
            console.log('Error connected to MongoDB database:', err.message);
            return;
        }
        db.collection('fb')
            .update({
                _id: parsedData.data.id
            }, {
                $set: {
                    category: parsedData.data.category,
                    name: parsedData.data.name,
                    location: parsedData.data.location,
                    about: parsedData.data.about,
                    overview: parsedData.data.company_overview,
                    cover: parsedData.data.cover.source,
                }
            }, {
                upsert: true
            }, (err, result) => {
                if (err) {
                    console.log('Error writing data to database:', err.message);
                    return;
                }
                console.log('Data written to database.');
                return;
            });
        client.close();
    });
}

function makeRequest(id) {
    https.get({
        hostname: 'jungcre-test.herokuapp.com',
        path: `/${id}/`,
        headers: {
            'fb-access-token': ACCESS_TOKEN
        }
    }, (response) => {
        const statusCode = response.statusCode,
            statusMessage = response.statusMessage;
    
        let error;
        if (statusCode !== 200) {
            error =  new Error(`API returned status code ${statusCode} with message '${statusMessage}'.`)
        }
    
        if (error) {
            console.log(error);
            response.resume();
            return;
        }
    
        response.setEncoding('utf8');
        let rawData = '';
        response.on('data', (chunk) => {
            rawData += chunk;
        });
    
        response.on('end', () => {
            const parsedData = JSON.parse(rawData);
            writeToDb(parsedData);
        });
    }).on('error', (err) => {
        console.log(err);
    });
}
